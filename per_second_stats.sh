#!/bin/bash

export DSTAT_MYSQL_USER=root
export DSTAT_MYSQL_PWD=<VLE MySQL Password>

dstat --time -clmnp --io --mysql5-cmds --mysql5-conn --mysql5-io --mysql5-keys --top-cpu --top-io --top-mem --nocolor $@