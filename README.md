### Ready!
Install/Enable Redhat EPEL repository if not installed/enabled already, 
```
# sudo yum install epel-release -y
```

### On your marks!
install dstat from EPEL repository if not already, 
```
# sudo yum install dstat -y
```

### Get set! 
Clone this repo so you get the .sh script, 
```
# git clone https://bitbucket.org/hassanshabbir/per-seconds-stats-via-dstat.git
```

### Go! 
Edit the .sh script and put a username password for MySQL that can query MySQL statistics, then execute the script like so,

```
# ./per_second_stats.sh
```

If along with just seeing the output on the screen, there is a need to dump it in a .csv do, 

```
# ./per_second_stats.sh --output <some file name.csv>
```